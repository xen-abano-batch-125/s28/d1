let http = require("http")
let PORT = 3000;

http.createServer((req,res)=>{
	//parts of request
		//uri/endpoint = resource
		//http method
		//body
		console.log(req);
	if (req.url ==="/profile" && req.method ==="GET"){
		//logic

		//before sending the response
		res.writeHead(200, {"Content-Type":"text/plain"})
		res.end("Welcome to my page")
	} else if (req.url ==="/profile" && req.method ==="POST"){
		res.writeHead(200, {"Content-Type":"text/plain"})
		res.end(`Data to be sent to the database`)
	} else {
		res.writeHead(404, {"Content-Type":"text/plain"})
		res.end(`Request cannot be completed.`)
	}

}).listen(PORT);

console.log(`Server is now connected to port ${PORT}`)